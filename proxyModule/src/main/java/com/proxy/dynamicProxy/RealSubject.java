package com.proxy.dynamicProxy;

/**
 * @author zhoubin
 * @descripter
 * @date2020/6/4
 */
public class RealSubject implements Subject {


    @Override
    public void request() {
        System.out.println("模拟请求");
    }
}
