package com.proxy.dynamicProxy;/**
 * @author zhoubin
 * @create 2020-06-04 下午12:55
 */

/**
 * @author zhoubin
 * @descripter
 * @date2020/6/4
 */
public interface Subject  {
    public void  request();
}
