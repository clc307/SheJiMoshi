package com.proxy.staticProxy;/**
 * @author zhoubin
 * @create 2020-06-04 下午12:43
 */

/**
 * @author zhoubin
 * @descripter 静态代理
 * @date2020/6/4
 */
public abstract class Subjects {

    public abstract void operate();
}
