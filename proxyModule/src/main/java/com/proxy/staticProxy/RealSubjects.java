package com.proxy.staticProxy;/**
 * @author zhoubin
 * @create 2020-06-04 下午12:43
 */

/**
 * @author zhoubin
 * @descripter
 * @date2020/6/4
 */
public class RealSubjects extends  Subjects {

    @Override
    public void operate() {
        System.out.println("do something..");
    }
}
