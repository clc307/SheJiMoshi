package com.proxy.staticProxy;/**
 * @author zhoubin
 * @create 2020-06-04 下午12:44
 */

/**
 * @author zhoubin
 * @descripter 代理模式
 * @date2020/6/4
 */
public class ProxySubjects extends  Subjects {


    private  RealSubjects real=new RealSubjects();

    @Override
    public void operate() {
        System.out.println("before....");
        real.operate();
        System.out.println("after....");
    }
}
