package com.proxy.staticProxy;/**
 * @author zhoubin
 * @create 2020-06-04 下午12:46
 */

/**
 * @author zhoubin
 * @descripter 静态代理客户端
 * @date2020/6/4
 */
public class Client {

    public static void main(String[] args) {
        Subjects subjectsa=new ProxySubjects();
        subjectsa.operate();
    }
}
